Thank you so much for downloading "Endear Vol.1 - Free Kawaii Midi Pack"!
I have been designed this pack for weeks!

All midis and synth loops are 100% royalty-free, they are all free to use.
But most of them are D Major scales or D# Major scales, you can change the scales when making your own music!

Note:
The Midi Pack includes all melodies and chords, so choose which one you want without music theories!

This pack includes:

- 20 Midis for Kawaii Genre
- 10 Synth Loops 
- Demo Song Kits

Have Fun!

JimVirslox
2023.3