Q1: Are all these samples royalty-free?
Answer: Yes, they are all 100% royalty- free, you can use them in your own music production.
              Also you can send me your demo if you use this pack！

Q2: I saw that some of the loops that in your songs, can I use them?
Answer: Yes, you can use them. But you can change the details of the loops, such as chop it or change the pitch.

Q3: What's the difference between the premium pack and the free edition?
Answer: Well the free edition is a big thank gift for my 200+ subscribers, it will be include lots of sample that I often use or I made them by myself.
              The Premium Edition will include more presets, projects, anime vocals, vocal chops and so on.

Q4: My friend want to use this pack too, can I share this to him or her?
Answer: Yes, feel free to share the pack (free edition) to your friends who don't own this pack, but you should share this video link.
               
Q5: Can I sell this pack to the others?
Answer: No you can't. You are NOT to sell this pack to this others especially the free dition! It's not fair to the others!